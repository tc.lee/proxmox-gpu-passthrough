#!/bin/sh
DEVICE1=`lspci -nn  | grep -oP 'VGA.*NVIDIA.*\[\K[\w:]+'`
modprobe vfio-pci
for dev in "0000:$DEVICE1"; do
        vendor=$(cat /sys/bus/pci/devices/$dev/vendor)
        device=$(cat /sys/bus/pci/devices/$dev/device)
        if [ -e /sys/bus/pci/devices/$dev/driver ]; then
                echo $dev > /sys/bus/pci/devices/$dev/driver/unbind
        fi
        echo $vendor $device > /sys/bus/pci/drivers/vfio-pci/new_id
done