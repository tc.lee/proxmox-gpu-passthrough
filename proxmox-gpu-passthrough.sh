cat << 'FOL' > proxmox-gpu-passthrough.sh
#!/bin/bash

# backlog
# check or prompt if it's nvidia card or other pcie cards
# double check all the options what does it all mean
# check if it's a ZFS system before running zfs items
# pciid , skip if pciid is empty
# must be root user

echo "Created by FreeSoftwareServers.com"
echo "##################################"
# Change Log
# 1.0.0.0 Can be used on most debian and rhel based linux,tested on Intel CPU only,  tested on GTX 1080 TI , RTX 2080 TI , RTX 2080 Super, Quadro 4000 , Tango Charlie 2020

# only root user
# --------------
if [ `whoami` != "root" ]; then
  echo "ERROR: You need to be root to run this script."
  exit 1
fi

# check system support for interrupt remapping 
# -------------------------------------------
if [ $(dmesg | grep ecap | wc -l) -eq 0 ]; then
  echo "No interrupt remapping support found"
  exit 1
fi

# check system support for interrupt remapping 
# -------------------------------------------
for i in $(dmesg | grep ecap | awk '{print $NF}'); do
  if [ $(( (0x$i & 0xf) >> 3 )) -ne 1 ]; then
    echo "Interrupt remapping not supported"
    exit 1
  fi
done


# SET PCI ID's!
# ---------------
pciid1="$(lspci -nn  | grep -oP 'VGA.*NVIDIA.*\[\K[\w:]+')"
pciid2="$(lspci -nn  | grep -oP 'Audio.*NVIDIA.*\[\K[\w:]+')"
pciid3="$(lspci -nn  | grep -oP 'USB.*NVIDIA.*\[\K[\w:]+')"
pciid4="$(lspci -nn  | grep -oP 'Serial.*NVIDIA.*\[\K[\w:]+')"



# Writing Card info into VFIO
# ------------------------
echo "Setting modprobe to use VFIO for NVidia PCI ID's"
echo ""
cat << EOF > /etc/modprobe.d/vfio.conf 
options vfio_iommu_type1 allow_unsafe_interrupts=1
options kvm ignore_msrs=1
options kvm report_ignored_msrs=0
options vfio-pci ids=$pciid1,$pciid2,$pciid3,$pciid4
EOF
cat /etc/modprobe.d/vfio.conf


# Blacklisting default drivers 
# ------------------------
echo "Blacklisting nouveau Drivers"
echo ""
cat << EOF > /etc/modprobe.d/blacklist.conf 
blacklist nouveau
blacklist radeon
blacklist nvidia
blacklist snd_hda_intel
blacklist i2c-nvidia-gpu
blacklist nvidiafb
EOF
cat /etc/modprobe.d/blacklist.conf 



# Update grub boot load options ...... list of it
# ------------------------
if [ "$(cat /proc/cpuinfo  | grep intel)" ]; then
echo ""
echo "Intel CPU Found Setting intel_iommu=on"
echo "Configure Grub to Blacklist nouveau Drivers"
echo "Configure Grub to use vfio-pci Drivers"
sed -i 's,^\(GRUB_CMDLINE_LINUX[ ]*=\).*,\1"rhgb quiet intel_iommu=on vfio-pci.ids=$pciid1,$pciid2,$pciid3,$pciid4 iommu=pt rd.driver.pre=vfio-pci rd.driver.blacklist=nouveau pcie_acs_override=downstream,multifunction nofb nomodeset video=vesafb:off video=efifb:off",g' /etc/default/grub
sed -i '/GRUB_CMDLINE_LINUX_DEFAULT/s/^/#/g' /etc/default/grub
else
echo ""
echo "AMD CPU Found Setting amd_iommu=on"
echo "Configure Grub to Blacklist nouveau Drivers"
echo "Configure Grub to use vfio-pci Drivers"
sed -i 's,^\(GRUB_CMDLINE_LINUX[ ]*=\).*,\1"rhgb quiet amd_iommu=on vfio-pci.ids=$pciid1,$pciid2,$pciid3,$pciid4 iommu=pt rd.driver.pre=vfio-pci rd.driver.blacklist=nouveau pcie_acs_override=downstream,multifunction nofb nomodeset video=vesafb:off video=efifb:off",g' /etc/default/grub
sed -i '/GRUB_CMDLINE_LINUX_DEFAULT/s/^/#/g' /etc/default/grub
fi

cat /etc/default/grub | grep "GRUB_CMDLINE_LINUX"

ls /etc/modprobe.d/
/usr/sbin/update-grub
/usr/sbin/update-grub2

# Check if IOMMU and virtualization is turned on from dmesg ( a bit tough as it shows different messages )


# Update Modules
# ------------------------
cat << EOF > /etc/modules 
vfio
vfio_iommu_type1
vfio_pci
vfio_virqfd
EOF
cat /etc/modules 

# Update cmdline 
# -------------
content=`cat /etc/kernel/cmdline`
content="$content intel_iommu=on iommu=pt pcie_acs_override=downstream,multifunction nofb nomodeset video=vesafb:off video=efifb:off"
cat << EOF > /etc/kernel/cmdline 
$content
EOF
cat /etc/kernel/cmdline 

/etc/kernel/postinst.d/zz-pve-efiboot
pve-efiboot-tool refresh

while true; do
    read -p "Would You Like to Reboot Now? It is Required!" yn
    case $yn in
        [Yy]* ) reboot;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done
FOL
chmod +x proxmox-gpu-passthrough